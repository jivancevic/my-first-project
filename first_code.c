#include <stdio.h>

int main(void) {
	int a, b;
	a = 1;
	b = 5;
	
	for (int i = 0; i < b; i++) {
		a *= a;
		a++;
	}

	printf("a=%d", &a);
	
	return 0;
}